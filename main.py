# This is a sample Python script.
from variables import courses_url
import Browser
import json
import re
from datetime import datetime
import pytz
from bs4 import BeautifulSoup

reg_jsessionid = r';jsessionid[^\?]*'


def find_last_minute_courses(source: str):
    soup = BeautifulSoup(source, features = "html.parser")
    course_actions = soup.findAll('p', attrs={'class': 'courseAction'})
    lastminute_courses = [lastminute_course.parent for lastminute_course in course_actions]
    return lastminute_courses


def parse_link(element, attrs: dict):
    link = element.findAll("a", attrs=attrs, href=True)[0]
    link_url = re.sub(reg_jsessionid,'', link["href"])
    return link.text, f'https://www.thw-ausbildungszentrum.de/{link_url}'


def parse_data(last_minute_course_elements: list):
    result = []
    for element in last_minute_course_elements:
        kurslink = parse_link(element, attrs={"title": "Details zum Lehrgang"})
        last_minute_anmeldung = parse_link(element, attrs={"title": "Eine Reservierungsanfrage an die Bundesschule für diesen Kurs stellen"})
        last_minute_zeiten = element.findAll("dl", attrs={"class": "docData"})[0].findAll("dd")

        last_minute_course = {
            "standort": element.findAll("span", attrs={'class': 'metadata'})[0].text,
            "gelesen_am": f'{datetime.now(pytz.timezone("CET"))}',
            "kurs": kurslink[0],
            "kurs_link": kurslink[1],
            "meldung": last_minute_anmeldung[0],
            "anmeldung_link": last_minute_anmeldung[1],
            "beginn": last_minute_zeiten[0].text,
            "ende": last_minute_zeiten[1].text,
        }
        result.append(last_minute_course)
    return result


def read_page():
    browser = Browser.Browser()
    try:
        browser.new_page(courses_url)
        source = browser.get_page_source()
        last_minute_course_elements = find_last_minute_courses(source)
        last_minute_course_data = parse_data(last_minute_course_elements)
        output_json= json.dumps(last_minute_course_data, indent=4)
        with open("last_minute.json", "w", encoding='utf-8') as output:
            json.dump(last_minute_course_data, output, indent=4)
    finally:
        browser.close_browser()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    read_page()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
