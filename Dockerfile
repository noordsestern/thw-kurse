FROM ghcr.io/marketsquare/robotframework-browser/rfbrowser-stable:latest

COPY requirements.txt .
RUN python3 -m pip install --user -r requirements.txt -U
