# THW Kurse 
Skript, dass Weiterbildungskurse von der Webseite vom Technischen Hilfwerk parst und als JSON aufbereitet.

## Was geht?
Einmal am Tag werden alle Kurse *von der ersten Seite* gesammelt, zu denen es Last Minute Plätze gibt. Einzusehen sind die Daten unter https://noordsestern.gitlab.io/thw-kurse/last_minute.json

## Was kommt?
### Paging
Sollte es mehr Kurse geben als auf die erste Seite passen, dann werden die im Moment noch nicht gefunden. Das wird sich noch ändern.

### Alle Kurse sammeln
Nicht nur die Last Minute Kurse sondern *alle* Kurse sollen gesammelt werden.

## Warum nicht direkt auf der Webseite nachsehen?
Möchte man die Kursdaten automatisch weiterverarbeiten, reicht die Webseite nicht mehr aus. Deshalb die Aufbereitung der Kursdaten in ein technisches Datenformat wie JSON.
